# Compilatori laboratorio - (2) Translate the infix calculator notation  into an abstract syntax tree (AST)

## Spiegazione del programma: 
Il programma è l'analizzatore sintattico di una calcolatrice con notazione infissa che, seguendo una precisa grammatica, traduce l'input ottenenuto in un AST. Siccome si tratta di una struttura non banale, le tipologie di dato devono essere gestite in maniera corretta, precisando che gli **operatori unari** devono essere rappresentati tra parentesi tonde. Il programma può generare unicamente un AST.


## Compilazione: 

```
yacc -d exercise_2.y
lex exercise_2.l
gcc y.tab.c lex.yy.c -lfl
```

## File LEX:

Il file LEX contiene due macro: 

- INTEGERS, utilizzata per leggere numeri interi 

- EXIT, utilizzata per terminare il programma


## File YACC:

Il file YACC contiene:

- la definizione dei tipi per ogni struttura
- la grammatica per riconoscere la stringa
- la funzione newNode che crea un nuovo nodo senza figli
- la funzione addChild che aggiunge i figli corrispondenti al nodo
- la funzione printUnaryOperator che genera l'operatore unario
- la funzione printMultipleOperator che genera l'operatore binario
- la funzione printAST che stampa l'albero in maniera strutturata

## Esempi di codice **funzionante** con relativo output:

**Input**
```
+25*-(2-3)
```
**Output**
```
*
  (+)
    25
  (-)
    -
      2
      3
```

-------

**Input**
```
(-11+7)*(31-+12)
```
**Output**
```
*
  +
    (-)
      11
    7
  -
    31
    (+)
      12
```

## Esempi di codice **NON funzionante** con relativo output:

**Input**
```
+
```
**Output**

```
syntax error, unexpected '\n', expecting INTEGERS or '+' or '-' or '('
```

-------

**Input**
```
555***22
```
**Output**

```
syntax error, unexpected '*', expecting INTEGERS or '+' or '-' or '('
```
-------

**Input**
```
letter
```
**Output**

```
syntax error, unexpected invalid token, expecting INTEGERS or '+' or '-' or '('
```
