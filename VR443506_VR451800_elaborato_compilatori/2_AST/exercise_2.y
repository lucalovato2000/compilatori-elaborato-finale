%{
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>

	int space = 0;

	typedef struct AST {
		char* content; 
		int numchild;
		struct AST* l_leaf;
		struct AST* r_leaf;

	} AST;

	int yylex();
	int yyparse();
	void yyerror(char const* s);

	AST* newNode(char* character);

	void printAST();
	void addChild(AST* node, AST* l_leaf, AST* r_leaf);  
	char* printUnaryOperator(char character);
	char* printMultipleOperator(char character);
%}

%define parse.error verbose

/* union for choosing the type after the input value */
%union{
	char* string;
    struct AST* tree;
}

%token <string> INTEGERS
%type <tree> E T F

%%
S:  E'\n'  		{ printAST($1); printf("\n"); }
	;

E:   E'+'T      { $$ = newNode(printMultipleOperator('+')); addChild($$,$1,$3); }
	|E'-'T      { $$ = newNode(printMultipleOperator('-')); addChild($$,$1,$3); }
	|T          { $$ = $1; } 
	;  


T:   T'*'F      { $$ = newNode(printMultipleOperator('*')); addChild($$,$1,$3); }
	|T'/'F      { $$ = newNode(printMultipleOperator('/')); addChild($$,$1,$3); }
  	|T'%'F      { $$ = newNode(printMultipleOperator('%')); addChild($$,$1,$3); }
  	|F          { $$ = $1; }
 	;   

F: 	 INTEGERS   { $$ = newNode($1); } 
	|'-'F       { $$ = newNode(printUnaryOperator('-')); addChild($$,$2,NULL); }
  	|'+'F       { $$ = newNode(printUnaryOperator('+')); addChild($$,$2,NULL); }
  	|'('E')'    { $$ = $2; }
  	;

%%

void yyerror(char const* s) {
	fprintf(stderr, "%s\n", s);
}

void main(int argc, char** argv) {
	yyparse();
}

char* getpastr(char* s) {
	return strcpy((char* )malloc(strlen(s) + 1), s);
}

AST* newNode(char* string) {
	AST* newnode = (AST* ) malloc(sizeof(AST));
	newnode -> content = getpastr(string);
	newnode -> numchild = 0;
	newnode -> l_leaf = NULL;
	newnode -> r_leaf = NULL;

	return newnode;
	free(newnode);
}

void addChild(AST* node, AST* l_leaf, AST* r_leaf) {
	node -> l_leaf = l_leaf;
	node -> r_leaf = r_leaf;

	node -> numchild++;
}

char* printUnaryOperator(char character) {
	char* operator = malloc(5 * sizeof(*operator));
	operator[0] = '(';
	operator[1] = character;
	operator[2] = ')';

	return operator;
	free(operator);
}

char* printMultipleOperator(char character) {
	char* operator = malloc(5 * sizeof(*operator));
	operator[0] = character;

	return operator;
	free(operator);
}

void printAST(AST* node) {
	int i = 0;
	
	while (i < space) {
		printf("  ");
		i++;
	}

	if (node != NULL) {
		printf("%s", node -> content);
		
		if (node -> l_leaf != NULL) {
			printf("\n");

			space = space + 1;
			printAST(node -> l_leaf);  
		}
		
		if (node -> r_leaf != NULL) {
			printf("\n");
			
			space = space + 1;
			printAST(node -> r_leaf);
		}
		space = space - 1;
  	}
}
