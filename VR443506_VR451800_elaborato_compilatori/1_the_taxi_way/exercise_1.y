%{
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>

	int yyparse();
	int yylex();
	void yyerror(char const* s);
	void update(char* command, int number);
	void check();

	int pos_x, pos_y = 0, dest_x = 5, dest_y = 6;
%}

%define parse.error verbose

/* union for choosing the type after the input value */
%union{
    int integer;
    char* up;
	char* down;
	char* left;
	char* right;
	char* end;
	char* command;
	char* grammar;
}

%token <integer> NUMBER
%token <up> UP
%token <down> DOWN
%token <right> RIGHT
%token <left> LEFT
%token <end> END
%type <grammar> start line
%type <command> command
%type <integer> number

%%

lines: start lines | ;
start: line'\n'				{ check(); }
line: command number line	{ update($1, $2); }
| END						{ printf("Coordinates to reach the end position: [x: %d, y: %d]\n", dest_x, dest_y); }
;

command: UP		{ $$ = $1; }
	   | DOWN	{ $$ = $1; }
	   | RIGHT	{ $$ = $1; }
	   | LEFT	{ $$ = $1; }
;

number: NUMBER 	{ $$ = $1; }
;

%%

void yyerror(char const* s) {
	fprintf(stderr, "%s\n", s);
}

void main(int argc, char** argv) {
	yyparse();
}

/* check if user has arrived to the end position */
void check() { 
	if (pos_x == dest_x && pos_y == dest_y) {
		printf("Congratulations! You have arrived at the final position\n");
		exit(0);	// ends if the user wins
	}
	else {
		/* update the user coordinates */
		printf("Oh no! You didn't get to the end point! To get there, you missed: [ ");
		
		if (dest_x - pos_x > 0) {
			printf("RIGHT %d ", dest_x - pos_x);
		}
		else {
			printf("LEFT %d ", pos_x - dest_x);
		}
		if (dest_y - pos_y > 0) {
			printf("UP %d ", dest_y - pos_y);
		}
		else {
			printf("DOWN %d ", pos_y - dest_y);
		}
		printf("]\n");
	}
}

/* check commands and update the values */
void update(char* command, int number) {
	if (strcmp(strndup(command, 2), "UP") == 0) {
		pos_y += number;
	}
	else if (strcmp(strndup(command, 4), "DOWN") == 0 ) {
		pos_y -= number;
	}
	else if (strcmp(strndup(command, 5), "RIGHT") == 0) {
		pos_x += number;
	}
	else if (strcmp(strndup(command, 4), "LEFT") == 0) {
		pos_x -= number;
	}
	else {
		printf("Error: \n command: %s number: %d\n", command, number);
	}
}
